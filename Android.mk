# Automatically generated file. DO NOT MODIFY
#
# This file is generated by device/oneplus/lemonadep/setup-makefiles.sh

LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),lemonadep)

$(call add-radio-file-sha1-checked,radio/abl.img,9ef19b8b71ad772dfb43cea1b2664ac7b4b90dce)
$(call add-radio-file-sha1-checked,radio/aop.img,476de39308431b818b8e35cd2e30afe687d9fef1)
$(call add-radio-file-sha1-checked,radio/bluetooth.img,5a2ba72a128765412fb84754e1ca99a29602ec2b)
$(call add-radio-file-sha1-checked,radio/cpucp.img,9d3c42c2da4426b56ce7581f0952ba2061dc0cd8)
$(call add-radio-file-sha1-checked,radio/devcfg.img,6f5b856694002f02c28c6011edc2c01b1ebb5370)
$(call add-radio-file-sha1-checked,radio/dsp.img,634718247c9670c0c2aab842804ea74c3f274396)
$(call add-radio-file-sha1-checked,radio/engineering_cdt.img,13f5f33df779408b04cdfb3d78dd1ebbd3afb7b4)
$(call add-radio-file-sha1-checked,radio/featenabler.img,7251415011a15690106dc4167574140a175cb48f)
$(call add-radio-file-sha1-checked,radio/hyp.img,7702164756d2df47662a43ee5c28de7003857395)
$(call add-radio-file-sha1-checked,radio/imagefv.img,7813d0d39538c0e9522610a584f44fb4fd535edd)
$(call add-radio-file-sha1-checked,radio/keymaster.img,763c53fe20e3c3412dcf9537b7d5c79e374535a1)
$(call add-radio-file-sha1-checked,radio/modem.img,6d44c4c017d941b9a9ee4e5c07546f9bfa67909b)
$(call add-radio-file-sha1-checked,radio/multiimgoem.img,f4da424228b9ad78563fee992ef363148919138f)
$(call add-radio-file-sha1-checked,radio/oplus_sec.img,70c1fb07ea5d63af49361d57b2a5f5d22a7b5612)
$(call add-radio-file-sha1-checked,radio/oplusstanvbk.img,32ef079677475cba1153c9e2e6447edc93e4612b)
$(call add-radio-file-sha1-checked,radio/qupfw.img,a698f1dabffbb508ed739c7e928f93069b7a6e37)
$(call add-radio-file-sha1-checked,radio/qweslicstore.img,180cf2903b9872499d05eeef08c472575fa3225e)
$(call add-radio-file-sha1-checked,radio/shrm.img,3174e8c9486c6b1c13079d37e63e8a13bfc61e33)
$(call add-radio-file-sha1-checked,radio/splash.img,cef1d4886175660f305e78ab39b7782f8625e431)
$(call add-radio-file-sha1-checked,radio/tz.img,dcaf0afb9da71323a1e915e848885be19f796061)
$(call add-radio-file-sha1-checked,radio/uefisecapp.img,c301e956d824bfe369ad5dddb369a00323543474)
$(call add-radio-file-sha1-checked,radio/vm-bootsys.img,28413317a0bc5afd71bbdd6474af5848832faa30)
$(call add-radio-file-sha1-checked,radio/xbl.img,b553a5a771d7070ab7b35cf68e8f85ef29aaa9df)
$(call add-radio-file-sha1-checked,radio/xbl_config.img,d36ac95aeff8e346c8ede980f333bd37387c94f5)

endif
